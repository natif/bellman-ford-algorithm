// BellmanFord.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include<iostream>


using namespace std;

struct Edge {
    int v1,v2;
    int weight;
};

int _tmain(int argc, _TCHAR* argv[])
{
    Edge edge[10]= {
        0,1,3,
        1,2,5,
        1,3,6,
        1,4,1,
        3,2,7,
        2,3,4,
        3,4,9,
		3,1,3,
        4,0,1,
        4,3,1
    };

    int predecessor[5];
    int distance[5];

    for(int i=0;i<5;i++) {
        distance[i]=1000000;
        predecessor[i]=-1;
    }

    int startingVertex = 0;

    cout<<"Choose the vertex you want to start from: "<<endl;
    cin>>startingVertex;

    distance[startingVertex] = 0 ;

    for(int j=0;j<4;j ++) {
        for(int i=0; i < 10; i++) {
            if(distance[edge[i].v1] > distance[edge[i].v2] + edge[i].weight) {
                distance[edge[i].v1] = distance[edge[i].v2] + edge[i].weight;
                predecessor[edge[i].v1] = edge[i].v2;
            }
        }
    }

	for(int i=0; i < 5; i++) {
		cout<<"\n Predecessor[" << i << "]"<< " = " << predecessor[i]<<endl;
	}

	for(int i=0; i < 5; i++) {
		cout<<"\n Distance[" << i << "]"<< " = " << distance[i]<<endl;
	}

	cout<<endl<<endl;
    system("pause");
    return 0;
}

